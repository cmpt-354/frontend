import React from 'react'
import { useAddDepartmentMutation, useDeleteDepartmentMutation, useGetAllDepartmentsQuery, useUpdateDepartmentMutation } from '../../redux/services/department';
import DataTable from '../../components/dataTable';
import Skeleton from '@mui/material/Skeleton';
import { Backdrop, CircularProgress } from '@mui/material';
import { drawerWidth } from '../../shared/constants';
import { AddModal } from '../../components/addModal';

const columns = [
  { field: 'id', headerName: 'ID', width: 80},
  { field: 'Name', headerName: 'Name', width: 150, editable: true },
];

const form = {
    name: {
        type: 'text',
        label: 'Name'
    }
}

const Departments = ({addModalOpen, setAddModalOpen}) => {
  const { data: departments, error, isFetching, refetch } = useGetAllDepartmentsQuery()
  const [addDepartment] = useAddDepartmentMutation()
  const [updateDepartment] = useUpdateDepartmentMutation()
  const [deleteDepartment] = useDeleteDepartmentMutation()

  const onAdd = (name) => {
    addDepartment(name).then(() => {
        setAddModalOpen(false)
        refetch()
    })
  }

  return(
    <>
        <div style={{height: 'calc(100% - 64px)', width: '100%'}}>
            <DataTable 
                rows={departments} 
                columns={columns} 
                rowsPerPage={50}
                isFetching={isFetching}
                deleteRow={deleteDepartment}
                refetch={refetch}
                updateRow={updateDepartment}
            />
        </div>
        <AddModal 
            open={addModalOpen} 
            setOpen={setAddModalOpen}
            name='Department'
            onAdd={onAdd}
            form={form}
        />
    </>
  )
}

export default Departments