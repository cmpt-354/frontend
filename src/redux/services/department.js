import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

// Define a service using a base URL and expected endpoints
export const departmentApi = createApi({
  reducerPath: 'pokemonApi',
  baseQuery: fetchBaseQuery({ baseUrl: 'http://localhost:5000/' }),
  endpoints: (builder) => ({
    getAllDepartments: builder.query({
      query: () => 'department/all/',
    }),
    addDepartment: (builder).mutation({
      query: ({ id, ...post }) => ({
        url: `department/post/`,
        method: 'POST',
        body: post,
      }),
    }),
    updateDepartment: (builder).mutation({
      query: ({ id, ...patch }) => ({
        url: `department/${id}/`,
        method: 'PATCH', 
        body: patch,
      }),
    }),
    deleteDepartment: (builder).mutation({
      query: (id) => ({
        url: `department/${id}/`,
        method: 'DELETE',
      }),
    })
  }),
})

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const { useGetAllDepartmentsQuery, useAddDepartmentMutation, useDeleteDepartmentMutation, useUpdateDepartmentMutation } = departmentApi