import { drawerLinks, equipmentLinks } from "./constants"

export const getLocationName = (pathname) => {
  return [...drawerLinks, ...equipmentLinks]
    .find(item => item.path === pathname)?.name || 'Not Found'
  // switch(pathname){
  //   case '/':
  //     return 'Home'
  //   case '/departments':
  //     return 'Departments'
  //   default:
  //     return 'Not Found'
  // }
}