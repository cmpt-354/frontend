
export const drawerLinks = [
  { name: 'Home', path: '/' },
  { name: 'Departments', path: '/departments' }
]

export const equipmentLinks = [
  { name: 'Camera', path: '/camera' },
  { name: 'Computer', path: '/computer' },
  { name: 'Default Device', path: '/defaultDevice' },
  { name: 'Gel', path: '/gel' },
  { name: 'Light', path: '/light' },
  { name: 'Lightning', path: '/lightning' },
  { name: 'Microphone', path: '/mic' },
  { name: 'Monitor', path: '/monitor' },
  { name: 'Projector', path: '/projector' },
  { name: 'Sound', path: '/sound' },
]

export const drawerWidth = 250;