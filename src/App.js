import { AppBar, Box, CssBaseline, Divider, Drawer, IconButton, List, ListItem, Toolbar, Typography } from "@mui/material";
import { Link, Route, Switch, withRouter } from "react-router-dom";
import Departments from "./routes/departments";
import MenuIcon from '@mui/icons-material/Menu';
import { useState } from "react";
import { useLocation } from 'react-router-dom'
import DrawerItems from "./components/drawerItems";
import { drawerWidth } from "./shared/constants";
import { getLocationName } from "./shared/functions";
import Home from "./routes/home";
import AddIcon from '@mui/icons-material/Add';

const App = (props) => {
  const [mobileOpen, setMobileOpen] = useState(false);
  const [addModalOpen, setAddModalOpen] = useState(false)
  const { history } = props
  const location = useLocation();

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const handleAddModal = () => {
    setAddModalOpen(true)
  }

  const container = window !== undefined ? () => window.document.body : undefined;

  const locationName = getLocationName(location.pathname)

  return (
    <Box sx={{ display: 'flex', height: '100%' }}>
      <CssBaseline />
      <AppBar
        position="fixed"
        sx={{
          width: { md: `calc(100% - ${drawerWidth}px)` },
          ml: { md: `${drawerWidth}px` },
        }}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            sx={{ mr: 2, display: { md: 'none' } }}
          >
            <MenuIcon />
          </IconButton>
          <Typography sx={{flexGrow: 1}} variant="h6" noWrap component="div">
            {locationName || 'Not Found'}
          </Typography>
          {locationName && locationName !== 'Home' &&
            <IconButton
                size="large"
                aria-label={`Add ${locationName}`}
                aria-controls="menu-appbar"
                // aria-haspopup="true"
                onClick={handleAddModal}
                color="inherit"
            >
                <AddIcon />
            </IconButton>
          }
        </Toolbar>
      </AppBar>
      <Box
        component="nav"
        sx={{ width: { md: drawerWidth }, flexShrink: { sm: 0 } }}
        aria-label="mailbox folders"
      >
        <Drawer
          container={container}
          variant="temporary"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          sx={{
            display: { xs: 'block', md: 'none' },
            '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
          }}
        >
          <DrawerItems history={history} />
        </Drawer>
        <Drawer
          variant="permanent"
          sx={{
            display: { xs: 'none', md: 'block' },
            '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
          }}
          open
        >
          <DrawerItems history={history} />
        </Drawer>
      </Box>
      <Box
        component="main"
        sx={{ flexGrow: 1, bgcolor: 'background.default', p: 3 }}
      >
        <Toolbar />
        <Switch>
          <Route exact path='/' render={() => <Home />} />
          <Route exact path='/departments' render={() => <Departments setAddModalOpen={setAddModalOpen} addModalOpen={addModalOpen} />} />
          <Route
            render={ () => <div>CMPT 354 Project</div> }
          />
        </Switch>
      </Box>
    </Box>
  );
}

export default withRouter(App)
