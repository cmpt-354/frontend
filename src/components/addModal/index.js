import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { useEffect, useState } from 'react';

export const AddModal = ({open, setOpen, onAdd, name, form}) => {
  const [submitValues, setSubmitValues] = useState({})

  const handleClose = () => {
    setOpen(false);
  };

  const handleAdd = () => {
    setSubmitValues({})
    onAdd(submitValues)
  }

  const isDisabeld = () => {
    
  }

  return (
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Add {name}</DialogTitle>
        <DialogContent>
          <DialogContentText sx={{marginBottom: '1rem'}}>
            To add a new <span className='bold'>{name}</span>
          </DialogContentText>
          { [...Object.keys(form)].map((key) => (
            <TextField
              key={key}
              autoFocus
              margin="dense"
              id={key}
              label={form[key].label}
              type={form[key].type}
              sx={{width: 400}}
              variant="outlined"
              value={submitValues[key]}
              onChange={(e) => setSubmitValues({...submitValues, [key]: e.target.value})}
            />
          ))

          }
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button variant='contained' onClick={handleAdd}>Add</Button>
        </DialogActions>
      </Dialog>
  );
}
