import React, { useState } from 'react'
import { DataGrid } from '@mui/x-data-grid';
import { Backdrop, Button, CircularProgress, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, TextField, Typography } from '@mui/material';
import { drawerWidth } from '../../shared/constants';
import LoadingButton from '@mui/lab/LoadingButton';

const DeleteModal = ({open, selectedCount, handleClose, onDelete, loading}) => {
  return (
     <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Confirm Action</DialogTitle>
        <DialogContent sx={{width: '400px'}}>
        <DialogContentText sx={{marginBottom: '1rem'}}>
            To delete <span className='bold'>{selectedCount}</span> rows
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <LoadingButton loading={loading} variant='contained' color='error' onClick={() => onDelete()}>Delete</LoadingButton>
        </DialogActions>
      </Dialog>
  )
}

const GridToolbar = ({onDelete, selectedCount}) => {
  const disabled = !selectedCount || selectedCount === 0
  return (
    <div style={{display: 'flex', padding: '1rem', justifyContent: 'start'}}>
      <Button 
        disabled={disabled} 
        variant="outlined" color="error"
        onClick={onDelete}
      >
        { disabled
          ? 'No Rows Selected'
          : `Delete ${selectedCount} ${selectedCount === 1 ? 'row' : 'rows'}`
        }
      </Button>
    </div>
  )
}

const DataTable = ({columns, rows, rowsPerPage, isFetching, deleteRow, refetch, updateRow}) => {
  const [selectedRows, setSelectedRows] = useState([])
  const [deleteOpen, setDeleteOpen] = useState(false)
  const [deleteLoading, setDeleteLoading] = useState(false)

  const onDelete = () => {
    setDeleteLoading(true)
    Promise.all(selectedRows.map((id) => 
      deleteRow(id)
    )).then(() => {
      setDeleteLoading(false)
      setDeleteOpen(false)
      setSelectedRows([])
      refetch()
    })
  }

  const onUpdate = ({id, field, formattedValue}) => {
    updateRow({
      id,
      [field]: formattedValue
    })
  }

  if(isFetching) {
    return (
      <Backdrop
        sx={{ color: '#fff'}}
        open={isFetching}
      >
        <CircularProgress sx={{marginLeft: {md: `${drawerWidth}px`}}} color="inherit"/>
      </Backdrop>
    )
  }
  return (
      <>
        <DataGrid
          rows={rows || ""}
          columns={columns}
          pageSize={rowsPerPage}
          rowsPerPageOptions={[rowsPerPage]}
          checkboxSelection
          // onCellEditCommit={(e) => console.log(e)}
          onCellEditStop={onUpdate}
          // onRowEditCommit={(e) => console.log(e)}
          onSelectionModelChange={setSelectedRows}
          experimentalFeatures={{ newEditingApi: true }}
          components={{
            Toolbar: GridToolbar
          }}
          componentsProps={{
            toolbar: {
              selectedCount: selectedRows.length,
              onDelete: () => setDeleteOpen(true)
            }
          }}
          disableSelectionOnClick
        />
        <DeleteModal 
          handleClose={() => setDeleteOpen(false)} 
          open={deleteOpen} 
          selectedCount={selectedRows.length} 
          loading={deleteLoading}
          onDelete={onDelete}
        />
      </>
  );
}

export default DataTable