import { Divider, List, ListItem, Toolbar } from '@mui/material';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import { drawerLinks, equipmentLinks } from '../../shared/constants';

const DrawerItems = ({history}) => {
  return (
    <div>
      <Toolbar />
      <Divider />
      <List>
        {drawerLinks.map((item) => (
          <ListItem key={item.name} disablePadding>
            <ListItemButton onClick={() => history.push(item.path)}>
              <ListItemText primary={item.name} />
            </ListItemButton>
          </ListItem>
        ))}
        <Divider />
        {equipmentLinks.sort(((a, b) => a.name - b.name)).map((item) => (
          <ListItem key={item.name} disablePadding>
            <ListItemButton onClick={() => history.push(item.path)}>
              <ListItemText primary={item.name} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    </div>
  );
}

export default DrawerItems