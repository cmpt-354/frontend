# Frontend

Frontend repository

## How to Run

Make sure that you have node installed.

If you don't have node, you can download it [HERE](https://nodejs.org/en/download/)

Run the following commands:

```
npm install
npm run
```

Done! The client is now accessable on ` http://127.0.0.1:3000/  `